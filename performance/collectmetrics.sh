#! /bin/bash
duration=1500
end=$((SECONDS+duration))
iter=1
while [ $SECONDS -lt $end ]; do
    kubectl get --raw /apis/metrics.k8s.io/v1beta1/nodes | jq > nodemetrics$iter.json
    kubectl get --raw /apis/metrics.k8s.io/v1beta1/namespaces/openfaas-fn/pods | jq > podmetrics$iter.json 
    kubectl top nodes > nodetop$iter.txt
    iter=$((iter+1))
    sleep 15
done
