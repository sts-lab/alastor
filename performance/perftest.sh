set -e -o pipefail

export OPENFAAS_URL=http://127.0.0.1:31112

nworker=10
duration=120s

#echo 'deploying hello-retail for single pod experiment'
#faas-cli deploy -f ../HR-singlepod.yaml

#echo 'deploying vanilla hello-retail for single pod experiment'
#faas-cli deploy -f ../HR-vanilla-singlepod.yaml

echo 'deploying hello-retail at 1-50-20'
faas-cli deploy -f ../hello-retail.yaml

#echo 'deploying vanilla hello-retail at 1-50-20'
#faas-cli deploy -f ../hello-retail-vanilla.yaml

echo 'waiting for hello-retail to come up...'
sleep 45

sh collectmetrics.sh &

for reqpersec in {1,5,10,15,20,25,30,35,40,45,50}
do
    #echo test-worker$nworker.reqpersec$reqpersec.txt
    echo 'launching hey for load testing'
    ../hey_linux_amd64 -z=$duration -q $reqpersec -c $nworker -m POST -D ../benign.json -H "Content-Type: application/json" http://127.0.0.1:31112/function/product-purchase > testperf-worker$nworker.reqpersec$reqpersec.txt
done

echo 'stabilize the cluster'
sleep 180
sh ../attack-kubernetes/tearHR.sh
