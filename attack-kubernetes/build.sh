#! /bin/bash

services='attack-product-purchase-authorize-cc attack-product-purchase attack-product-purchase-get-price attack-product-purchase-publish' #attackserver cc-db''

#services='attack-product-purchase'

for service in $services
do
	tag="dattapubali/${service}:latest"
	echo Building $tag
	(cd $service && cp /shared/mitmproxy/mitmproxy-ca-cert.pem . && docker build -t $tag .)
    docker push $tag
done

#for service in $services
#do
#	old_tag="${service}:latest"
#	new_tag="dattapubali/${service}:latest-attack"
#	echo Retagging $old_tag to $new_tag
#	docker tag $old_tag $new_tag
#done