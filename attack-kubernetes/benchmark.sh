#! /bin/bash
multitime -n 100 curl 'http://127.0.0.1:31112/function/product-purchase' -H "Content-type:application/json" -X POST -d "@benign.json"
sleep 10
multitime -n 100 curl 'http://127.0.0.1:31112/function/product-purchase' -H "Content-type:application/json" -X POST -d "@attack1.json"
sleep 10
multitime -n 100 curl 'http://127.0.0.1:31112/function/product-purchase' -H "Content-type:application/json" -X POST -d "@attack2.json"