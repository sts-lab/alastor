#! /bin/bash

set -e -o pipefail

export OPENFAAS_URL=http://127.0.0.1:31112

echo 'deploying hello-retail'
faas-cli deploy -f ./hello-retail-vanilla.yaml

echo 'waiting for hello-retail to come up...'
sleep 30

echo 'ready to test attack hello-retail workflows'