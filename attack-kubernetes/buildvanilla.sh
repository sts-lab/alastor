#! /bin/bash

services='attack-product-purchase-authorize-cc attack-product-purchase attack-product-purchase-get-price attack-product-purchase-publish'

for service in $services
do
	tag="dattapubali/vanilla-${service}:latest"
	echo Building $tag
	(cd $service && docker build -t $tag -f Dockerfile.vanilla .)
    docker push $tag
done

#for service in $services
#do
#	old_tag="${service}:latest"
#	new_tag="dattapubali/${service}:latest-attack"
#	echo Retagging $old_tag to $new_tag
#	docker tag $old_tag $new_tag
#done