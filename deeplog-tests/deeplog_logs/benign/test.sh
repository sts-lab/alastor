set -e -o pipefail
var="log"
reqStartTime=$(grep -m 1 "Request ID" request.alastor.$var | cut -d' ' -f 1-2)
echo "${reqStartTime}"
cdtStartTime=$(TZ=UTC date -d "$reqStartTime" +"%s")
echo "${cdtStartTime}"
#linenum=$(grep -m 1 -n "${cdtStartTime}" request.alastor.18 | cut -f1 -d:)
#truncate="$(($linenum-1))"
#echo $truncate
#sed -i 1,$(($linenum-1))d request.alastor.18
#awk -v var="$cdtStartTime" '$0 > var' request.alastor.18
awk -v var="$cdtStartTime" '$0 > var' request.alastor.18 > tmp && mv -f tmp request.alastor.18