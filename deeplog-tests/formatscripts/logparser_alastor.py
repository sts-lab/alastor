# THIS CODE WORKS WITH THE logparser LIBRARY using the method -
# https://logparser.readthedocs.io/en/latest/demo.html 
# After running the above docker container, docker cp this file and the logfiles
# to generate structures logs 

#!/usr/bin/env python
import sys
sys.path.append('../')
from logparser import Spell

input_dir  = '../logs/alastor/'  # The input directory of log file
output_dir = 'Spell_result/'  # The output directory of parsing results
log_file   = 'alastor.log'  # The input log file name
log_format = '<Time> <Content> = <RetVal>'  # alastor log format
tau        = 0.5  # Message type threshold (default: 0.5)
regex      = []  # Regular expression list for optional preprocessing (default: [])

parser = Spell.LogParser(indir=input_dir, outdir=output_dir, log_format=log_format, tau=tau, rex=regex)
#parser = Spell.LogParser(indir=input_dir, outdir=output_dir, tau=tau, rex=regex)
parser.parse(log_file)
