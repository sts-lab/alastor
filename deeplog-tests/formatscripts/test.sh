set -e -o pipefail
var="log"
reqStartTime=$(grep -m 1 "Request ID" request.alastor.$var | cut -d' ' -f 1-2)
echo "${reqStartTime}"
cdtStartTime=$(TZ=UTC date -d "$reqStartTime + 6 hours" "+%Y-%m-%d %H:%M:%S")
echo "${cdtStartTime}"