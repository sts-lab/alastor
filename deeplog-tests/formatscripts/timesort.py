# !!!! THIS CODE WILL NOT WORK BECAUSE with whitespace as separator the strace
# log rows will have variable lengths

# importing pandas package
import pandas as pd
import glob

alastorLogs = glob.glob('request.alastor.*')

listdf = []
# assign dataset
for logfile in alastorLogs:
    csvData = pd.read_csv(logfile, header=None, delim_whitespace=True)
    listdf.append(csvData)

mergedlog = pd.concat(listdf)
                                         
# displaying unsorted data frame
print("\nBefore sorting:")
print(csvData)
  
# sort data frame
csvData.sort_values(["Salary"], 
                    axis=0,
                    ascending=[False], 
                    inplace=True)
  
# displaying sorted data frame
print("\nAfter sorting:")
print(csvData)