# This script untars all tar files
# merges the strace logs and sort them according to epoch timestamp
# converts the sorted file's date time format to be compatible with DeepLog
# Takes the input file directory and the final output file name as input
# The final output is found in the input file directory
set -e -o pipefail

(
cd $1
#rm $2

for file in *.tar
do 
    tarid=${file:${#file}-9:5} # extracts the unique part of the tar file name
    #filename=${file%.*} #filename without the extension
    echo $tarid
    tar -xf "$file" --transform "s,$,.$tarid," # appends the tarid to every extracted filename
    
    #from the info log (request.alastor.log) get the timestamp for first request
    #scheduled to omit strace logs corresponding to setup process for better training
    reqStartTime=$(grep -m 1 "Request ID" request.alastor.log.$tarid | cut -d' ' -f 1-2)
    rm request.alastor.log* # removes the info logs

    #convert the request start time to CDT because the OpenFaas info log is in
    #UTC
    #cdtStartTime=$(TZ=UTC date -d "$reqStartTime + 6 hours" "+%Y-%m-%d
    #%H:%M:%S")
    
    #convert the request start time to epoch time
    epochStartTime=$(TZ=UTC date -d "$reqStartTime" +"%s")
    
    # truncates the log file events occuring before epochStartTime
    # Appends the tarid (the podid actually) after the timestamp field
    for logfile in *.$tarid
    do
        if [ -s $logfile ]; then
            echo $logfile
            sed -i "s/ / $tarid  /1" $logfile
            awk -v var="$epochStartTime" '$0 > var' $logfile > tmp && mv -f tmp $logfile
        fi
    done
done

cat request.alastor.* >> merged.log # merges the remaining strace log files
sort -k1 -n merged.log > sorted.log # sorts the files according to epoch timestamp

#convert the date time format
awk -F " " -v ORS=" " '{ printf strftime("%F %T."substr($1,12),$1) " "; for (i=2; i<NF; i++) print $i ; printf "%s\n", $NF }' sorted.log > $2

#clean up unnecessary files
rm request.alastor.* merged.log sorted.log
)