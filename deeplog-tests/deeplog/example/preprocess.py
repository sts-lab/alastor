import os
import sys
import logging
import pandas as pd
from spellpy import spell

logging.basicConfig(level=logging.WARNING,
                    format='[%(asctime)s][%(levelname)s]: %(message)s')
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler(sys.stdout))


def deeplog_df_transfer(df, event_id_map):
    df['datetime'] = pd.to_datetime(df['Date'] + ' ' + df['Time'])
    df = df[['datetime', 'EventId']]
    df['EventId'] = df['EventId'].apply(lambda e: event_id_map[e] if event_id_map.get(e) else -1)
    #deeplog_df =
    #df.set_index('datetime').resample('200ms').apply(_custom_resampler).reset_index()
    deeplog_df = df.set_index('datetime').resample('200ms').apply(_custom_resampler).reset_index()
    return deeplog_df


def _custom_resampler(array_like):
    return list(array_like)


def deeplog_file_generator(filename, df):
    with open(filename, 'w') as f:
        for event_id_list in df['EventId']:
            for event_id in event_id_list:
                f.write(str(event_id) + ' ')
            f.write('\n')


if __name__ == '__main__':
    ##########
    # Parser #
    ##########
    input_dir = './data/alastor/'
    output_dir = './alastor_result/'
    #log_format = '<Logrecord> <Date> <Time> <Pid> <Level> <Component> \[<ADDR>\] <Content>'
    log_format = '<Date> <Time> <PodID> <Content> = <RetVal>'
    log_main = 'alastor'
    tau = 0.5

    parser = spell.LogParser(
        indir=input_dir,
        outdir=output_dir,
        log_format=log_format,
        logmain=log_main,
        tau=tau,
    )

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

   # for log_name in ['alastor_abnormal.log', 'alastor_normal.log']:
    for log_name in ['alastor_benign.log','alastor_attack.log','alastor_benign1.log']:    
        parser.parse(log_name)

    ##################
    # Transformation #
    ##################
    df = pd.read_csv(f'{output_dir}/alastor_benign.log_structured.csv')
    df_benign = pd.read_csv(f'{output_dir}/alastor_benign1.log_structured.csv')
    df_attack = pd.read_csv(f'{output_dir}/alastor_attack.log_structured.csv')

    event_id_map = dict()
    for i, event_id in enumerate(df['EventId'].unique(), 1):
        event_id_map[event_id] = i

    logger.info(f'length of event_id_map: {len(event_id_map)}')

    #########
    # Train #
    #########
    deeplog_train = deeplog_df_transfer(df, event_id_map)
    deeplog_file_generator('train', deeplog_train)

    ###############
    # Test Benign #
    ###############
    deeplog_test_benign = deeplog_df_transfer(df_benign, event_id_map)
    deeplog_file_generator('test_benign', deeplog_test_benign)

    #################
    # Test Attack #
    #################
    deeplog_test_attack = deeplog_df_transfer(df_attack, event_id_map)
    deeplog_file_generator('test_attack', deeplog_test_attack)
