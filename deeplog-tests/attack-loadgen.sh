#! /bin/bash
###### REMEMBER TO RUN THE FILESERVER FIRST #######

set -e -o pipefail

export OPENFAAS_URL=http://127.0.0.1:31112

echo 'deploying hello-retail with no scaling to use logs for deeplog'
faas-cli deploy -f ../HR-singlepod.yaml

echo 'waiting for hello-retail to come up...'
sleep 30

nworker=2
duration=30s
reqpersec=5

#echo 'launching hey for load testing attack type 1 and 2'
#~/hey_linux_amd64 -z=$duration -q $reqpersec -c $nworker -m POST -D ./attack1.json -H "Content-Type: application/json" http://127.0.0.1:31112/function/product-purchase
#~/hey_linux_amd64 -z=$duration -q $reqpersec -c $nworker -m POST -D ./attack2.json -H "Content-Type: application/json" http://127.0.0.1:31112/function/product-purchase

echo "launching control flow attack"

~/hey_linux_amd64 -z=$duration -q $reqpersec -c $nworker -m POST -D ../attack-kubernetes/cfattack.json -H "Content-Type: application/json" http://127.0.0.1:31112/function/product-purchase

sleep 30

echo "Taking down functions and collecting logs"
sh ../attack-kubernetes/tearHR.sh