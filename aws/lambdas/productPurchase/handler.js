'use strict';

var AWS = require('aws-sdk');
AWS.config.region = 'us-east-2';
var lambda = new AWS.Lambda();

const util = require('util');

const epsagon = require('../node_modules/epsagon');
epsagon.init({
    token: process.env.EPSAGONTOKEN,
    appName: 'vulnerable-hello-retail',
    metaDataOnly: false,
});

async  function invokePromise(params) {
	return new Promise((resolve, reject) => {
	    lambda.invoke(params, function(err, data) {
		if (err) {
		    console.log('Error invoking function');
		    reject(err);
		} else {
		    console.log('Response received');
		    resolve(data);
		}
	    });
	});
    }


async function handler(event, context, callback) {
    console.log('Function productPurchase running.');

    var priceParams = {
	FunctionName: process.env.GETPRICE,
	InvocationType: 'RequestResponse',
	LogType: 'Tail',
	Payload: JSON.stringify({
	    id: event.id
	})
    };

    var authCCargs = {
	user: event.user,
	creditCard: event.creditCard
    }

    // Check if this is an exploit, propagate to authcc function
    if (event.malicious) {
	authCCargs.malicious = event.malicious;
	authCCargs.attackserver = event.attackserver;
    }

    var authCCParams = {
	FunctionName: process.env.AUTHCC,
	InvocationType: 'RequestResponse',
	LogType: 'Tail',
	Payload: JSON.stringify(authCCargs)
    };

    var priceRes;
    var authRes;

    await Promise.all([invokePromise(priceParams), invokePromise(authCCParams)]).then(res => {
	console.log(res);
	priceRes = JSON.parse(res[0].Payload);
	authRes = JSON.parse(res[1].Payload);
    });

    var publishArgs = {
	id: event.id,
	price: priceRes.price,
	user: event.user, 
	authorization: authRes.authorization,
    };

    if (priceRes.gotPrice == 'false') {
	publishArgs.approved = 'false';
	publishArgs.failureReason = priceRes.failureReason;
    } else if (authRes.approved == 'false') {
	publishArgs.approved = 'false';
	publishArgs.failureReason = authRes.failureReason;
    } else {
	publishArgs.approved = 'true';
    }

    var publishParams = {
	FunctionName: process.env.PUBLISH,
	InvocationType: 'RequestResponse',
	LogType: 'Tail',
	Payload: JSON.stringify(publishArgs)
    };

    await invokePromise(publishParams).then(res => {
	    var publishRes = JSON.parse(res.Payload);
	    var result = {};

	    if (publishRes.failureReason) {
		result.success = 'false';
		if (typeof publishRes.failureReason === 'string' || publishRes.failureReason instanceof String) {
		    result.failureReason = publishRes.failureReason;
		} else {
		    result.failureReason = { ...publishRes.failureReason };
		}
	    } else {
		result.success = 'true';
		result.chargedAmount = publishRes.productPrice;
		result.authorization = publishRes.authorization;
	    }

	    var finalResponse = {
		...result
	    };

	    if (false) {
		finalResponse.debug = {
		    getPrice: priceRes,
		    authCC: authRes,
		    publish: publishRes
		}
	    }

	    callback(null, finalResponse);
	});
};

const handlerPromise = util.promisify(handler);

module.exports.main = epsagon.lambdaWrapper(async (event, context, callback) => {
    await handlerPromise(event, context, callback);
});

