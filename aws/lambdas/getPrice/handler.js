'use strict';

const epsagon = require('../node_modules/epsagon');
epsagon.init({
    token: process.env.EPSAGONTOKEN, 
    appName: 'vulnerable-hello-retail',
    metaDataOnly: false,
});

function randomPrice() {
    var cents = Math.floor(Math.random() * 100);
    var dollars = Math.floor(Math.random() * 100);

    return (dollars + (cents * .01)).toFixed(2);
}

module.exports.main = epsagon.lambdaWrapper((event, context, callback) => {
    console.log('Function getPrice running.');

    console.log(event.id);

    if (Math.random() < 0.9) {
        const response = {
	    gotPrice: 'true',
	    price: randomPrice(),
	    devFinished: 'false: does not use database, generates random price.'
	};

	callback(null, response);
    } else {
	const response = {
	    gotPrice: 'false',
	    failureReason: 'No price in the catalog',
	    devFinished: 'false'
	};

	callback(null, response);
    }
});
