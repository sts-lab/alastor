# AWS Vulnerable Hello Retail
This directory contains everything needed to deploy the vulnerable functions, a web server to host the attack script, a database
server for credit card info, and then trigger benign and malicious work flows.

## Pre-reqs
Make sure to install the serverless CLI tool (sls) and the AWS CLI tools. Then configure them according to the instructions. Also the `jq`
bash command is used and typically not installed on machines.

`Note: I hardcoded us-east-2 in a couple places so if this is breaking that might be a reason why`

## Deployment instructions
Simply deploy the web server and DB first and then copy the command output by the deploy script and run it
in the lambda directory. You can either supply your epsagon token to the deploy script or manually add
it to the output yourself.

```
$ cd resources
$ ./deploy.sh [epsagontoken]
ATTACKSERVER='3.16.49.126' DBSERVER='3.137.150.117' EPSAGONTOKEN='token' bash -c './deployandtest.sh'

$ cd ../lambdas
$ ATTACKSERVER='3.16.49.126' DBSERVER='3.137.150.117' EPSAGONTOKEN='token' bash -c './deployandtest.sh'

$ sls remove
```

## Potential Issues

- If the EC2 instances don't have internet access make sure to check the rules on the VPC itself, manually allowing inbound and outbound access

- After removing the lambdas from the commandline, I like to manually terminate the EC2 instances and remove the security groups I created because I was too lazy to script this

- You might want to give the servers a minute to initialize before executing `deployandtest.sh`

## Detailed File Description

### resources/deploy.sh
This script assumes a properly configured AWS CLI and creates two security groups one to allow HTTP traffic and one to allow mysql traffic.
It then creates two m2.micro servers with the Amazon linux image supplying config files to setup a database server and web server. Finally
it outputs the command that should be executed in the lambda directory.

### resources/db.txt
Bash script to install mysql, setup the `abc` user, and initialize the database with a creditcards table and one row of data.

### resources/server.txt
Bash script to setup an apache HTTP server and host the attack script.

### lambdas/serverless.yml
Defintion of lambda functions, used by `sls` to deploy your functions.

### lambdas/deployandtest.sh
Calls `sls deploy` and then triggers a normal invocation, followed by the two malicious invocations, and finally another benign invocation.

### lambdas/dependencies/
Contains the `mysqldump` executable that is used in the attack script.

### lambdas/node\_modules/
Contains all the third-party node modules for the function handlers.
