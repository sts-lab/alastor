#!/bin/sh

sudo yum -y update
wget http://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm
sudo yum -y localinstall mysql57-community-release-el7-8.noarch.rpm
sudo yum -y install mysql-community-server expect
rm mysql57-community-release-el7-8.noarch.rpm

sudo systemctl start mysqld

DEFAULTPASS=$(sudo grep 'temporary password' /var/log/mysqld.log | gawk '{n=split($0,array," ")}; {print array[n]}')
NEWPASS='AlastorRootSecurePassword1^'

SECURE_MYSQL=$(expect -c "

set timeout 3
spawn mysql_secure_installation

expect \"Enter password for user root:\"
send \"$DEFAULTPASS\r\"

expect \"New password:\"
send \"$NEWPASS\r\"

expect \"Re-enter new password:\"
send \"$NEWPASS\r\"

expect \"Change the password for root ? ((Press y|Y for Yes, any other key for No) :\"
send \"n\r\"

expect \"Remove anonymous users? (Press y|Y for Yes, any other key for No) :\"
send  \"y\r\"

expect \"Disallow root login remotely? (Press y|Y for Yes, any other key for No) :\"
send \"y\r\"

expect \"Remove test database and access to it? (Press y|Y for Yes, any other key for No) :\"
send \"y\r\"

expect \"Reload privilege tables now? (Press y|Y for Yes, any other key for No) :\"
send \"y\r\"

expect eof
")

echo "${SECURE_MYSQL}"

mysql -u root -p"$NEWPASS" -h localhost -e 'UNINSTALL PLUGIN validate_password;'
mysql -u root -p"$NEWPASS" -h localhost -e 'CREATE DATABASE helloRetail;'
mysql -u root -p"$NEWPASS" -h localhost -e 'CREATE USER "abc"@"%" IDENTIFIED BY "xyz";'
mysql -u root -p"$NEWPASS" -h localhost -e 'GRANT ALL PRIVILEGES ON helloRetail . * TO "abc"@"%";'
mysql -u root -p"$NEWPASS" -h localhost -c helloRetail -e 'CREATE TABLE creditCards(rowkey VARCHAR(255) PRIMARY KEY, rowvalue VARCHAR(255) NOT NULL);' 
mysql -u root -p"$NEWPASS" -h localhost -c helloRetail -e 'INSERT INTO creditCards(rowkey, rowvalue) VALUES ("alice", "1234-5678-9123-4567");'

echo 'bind-address=0.0.0.0' | sudo tee -a /etc/my.cnf

sudo systemctl restart mysqld
