#!/bin/bash

HTTPGROUP=$(aws ec2 create-security-group --group-name httpserver --description "Allow port 80 from everywhere" | jq -r '.GroupId')
aws ec2 authorize-security-group-ingress --group-name httpserver --protocol tcp --port 80 --cidr 0.0.0.0/0

MYSQLGROUP=$(aws ec2 create-security-group --group-name mysqlserver --description "Allow port 3306 from everywhere" | jq -r '.GroupId')
aws ec2 authorize-security-group-ingress --group-name mysqlserver --protocol tcp --port 3306 --cidr 0.0.0.0/0

HTTPSERVERID=$(aws ec2 run-instances --image-id ami-0a0ad6b70e61be944 --security-groups httpserver --instance-type t2.micro --user-data file://server.txt | jq -r '.Instances[0].InstanceId')
DBID=$(aws ec2 run-instances --image-id ami-0a0ad6b70e61be944 --security-groups mysqlserver --instance-type t2.micro --user-data file://db.txt | jq -r '.Instances[0].InstanceId')

HTTPIP=$(aws ec2 describe-instances --instance-ids $HTTPSERVERID | jq -r '.Reservations[0].Instances[0].PublicIpAddress')
DBIP=$(aws ec2 describe-instances --instance-ids $DBID | jq -r '.Reservations[0].Instances[0].PublicIpAddress')

if [ -z "$1" ]
then
    TOKEN="<token>"
else
    TOKEN="$1"
fi


echo "ATTACKSERVER='$HTTPIP' DBSERVER='$DBIP' EPSAGONTOKEN='$TOKEN' bash -c './deployandtest.sh'" 
