#!/bin/bash

if [ ! "$http_proxy" = "" ]
then
    docker build --no-cache --build-arg "https_proxy=$https_proxy" --build-arg "http_proxy=$http_proxy" -t openfaas/of-watchdog:build .
else
    docker build -t openfaas/of-watchdog:build .
fi

docker build --no-cache --build-arg PLATFORM="" -t openfaas/of-watchdog:latest-dev-x86_64 . -f Dockerfile.packager

docker create --name buildoutput openfaas/of-watchdog:build echo

docker cp buildoutput:/go/src/github.com/openfaas/of-watchdog/of-watchdog ./of-watchdog

docker rm buildoutput
