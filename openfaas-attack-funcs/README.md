# Vulnerable helloRetail! Proof of Concept

## Quick Guide
```
docker-compose build --parallel
docker-compose up
```

Make sure you have manually built of-watchdog as it is not built automatically
but required for the functions. Build the watchdog by running the following command
in the `of-watchdog` directory.

```
./build.sh
```

### Normal Execution

```
curl -d @benign.json -H "Content-Type: application/json" -X POST 'http://localhost:8080/'
```

### Malicious Executions

Exfiltration attack with warm container reuse (note might need to update the attack server in attack1.json)
```
curl -d @attack1.json -H "Content-Type: application/json" -X POST 'http://localhost:8080/'
curl -d @attack2.json -H "Content-Type: application/json" -X POST 'http://localhost:8080/'
```

Business Logic/Flow Manipulation attack that skips cc authorization and immediately purchases a product.
```
curl -d @cfattack.json -H "Content-Type: application/json" -X POST 'http://localhost:8080/'
```

## Description

### High Level Overview

The goal was to introduce an attack workflow that highlighted two key components: pivoting from a public facing function to
an internal function with access to sensitive data and exploiting warm container reuse. To implement this vulnerable workflow
I simply hijack execution if "malicious" is sent in the request body. The modified functions simulated a RCE vulnerability
allowing the attacker to perform the malicious actions. If the request do not contain "malicious" in the request body, the
functions operate normally. 

The most sensitive data in the helloRetail! application is the credit card info stored in the Credit Card Registry (D4). To
access this data the attack must first go through the public facing Purchase Product (f9) function and then pivot to the
Authorize Credit Card (f12) function. In the first request the attacker has the goal of installing attack scripts onto
f12, first the attacker exploits the vulnerability in f9 and then pivots to f12 and injects code to download an attack script
from a server they control. This attack script is stored locally on the function and then in the attacker's second request
they exploit f9 again and pivot to f12 but this time they execute the script downloaded in the previous request. Once executed,
the script on f12 dumps the data from D4 and returns the credit card information in the HTTP response from f12 to f9, f9 takes this
response and returns it to the attacker.

To exploit the vulnerability the attacker only needs to send the `malicious` JSON key in the request body
with the value `one` for step one and the value `two` for step two and the `attackserver` key with the address of the server
hosting the attack script.

### Detailed Explanation

#### attackserver/
Stands up an apache server on port 80 hosting the "attack script" which is simply a bash script calling mysqldump

Listens on localhost:8888 for testing externally

#### cc-db/
Stands up a mysql server and creates and empty "helloRetail" database

Important environment variables that must be set:
MYSQL\_ROOT\_USER and MYSQL\_ROOT\_PASSWORD - both are self-explanatory and should match the variables set in
product-purchase-authorize-cc

#### docker-compose.yaml
Sets up all the containers on a shared network to enable testing

#### malicious.js
Simple helper library that I use in product-purchase-authorize-cc

#### of-watchdog/
Source code directly taken from openfaas repo. Simply run ./build.sh to create the of-watchdog (note I only have x86\_64 build enabled)

#### product-purchase/
Public facing entrypont (f9) that simply allows pivoting to internal functions

Important environment variables that must be set:

- URL\_GETPRICE - full URL for get-price function
- URL\_AUTHORIZECC - full URL for authorize-cc function
- URL\_PUBLISH - full URL for publish function

Listens on localhost:8080 for invoking externally

#### product-purchase-authorize-cc/
Internal function (f12) that has direct access to the credit card database.

Slightly modified to remove DB access, assumes you will always pass `creditCard` in the request.

Important environment variables that must be set:

- DBNAME - Name of the database to use
- HOST - Address/hostname of database
- USER - MySQL username
- PASS - MySQL password
- TABLE\_CREDIT\_Cards\_NAME - Name of table holding credit card info

Listens on localhost 8081 for testing directly without going through f9.

Make sure to add the following line to the Dockerfile to install mysqldump: `apk --no-cache add mysql-client` 

#### product-purchase-publish/
Internal function (f13) that's supposed to display the purchase or something publicly. Slightly modified to just return
a result back to the user. No DB interaction

Listens on localhost 8082 for testing directly without going through f9.

#### product-purchase-get-price/
Internal function (f11).

Slightly modified to return a random price. Can randomly fail. Has no DB access

Listens on localhost 8083 for testing directly without going through f9.

#### benign.json
Sample json data file used for testing f12

#### attack1.json
Sample json data for downloading attack scripts to f12.

#### attack2.json
Sample json data for exfiltrating data from the database using f12.


## Miscellaneous Notes

- The helloRetail! application requires of-watchdog and I do not build the
  watchdog in the docker-compose file.

- I assumed the database would be MySQL

- I copied the of-watchdog from the official of-watchdog repo, no modification to the watchdog was necessary. Just ensure your
environment has a watchdog labeled `openfaas/of-watchdog:latest-dev-x86_64` or update the function Dockerfiles to reflect the 
version of the watchdog you want to use.

- To simplify the database dump, I installed mysqldump (in the mysql-client package) on the Authorize Credit Card function container.
It is reasonable to imagine that the executable could have been downloaded during step 1, but I was having technical issues. Otherwise
using the credentials saved in the environment it could also be possible to inject a simple SELECT * statement to achieve the same
results.

- It may also be useful to set exec\_timeout to `0s` if functions are taking too
  long and being killed by the watchdog.

- If applying this logic to other functions, you need to make sure you modify the existing code to make the function definitions async.
I'm actually not very familiar with javascript and in places I forced synchronous actions although I probably could have just passed
the callback to achieve the same outcome.
