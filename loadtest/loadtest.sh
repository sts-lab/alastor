#! /bin/bash
###### REMEMBER TO RUN THE FILESERVER FIRST #######

set -e -o pipefail

export OPENFAAS_URL=http://127.0.0.1:31112

nworker=10
duration=120s

for reqpersec in {1,5,10,15,20,25,30,35,40,45,50}
do
    echo test-worker$nworker.reqpersec$reqpersec.txt
    echo 'deploying hello-retail'
    faas-cli deploy -f ../attack-kubernetes/hello-retail.yaml

    echo 'waiting for hello-retail to come up...'
    sleep 45

    echo 'launching hey for load testing'
    ./hey_linux_amd64 -z=$duration -q $reqpersec -c $nworker -m POST -D ./benign.json -H "Content-Type: application/json" http://127.0.0.1:31112/function/product-purchase > test-worker$nworker.reqpersec$reqpersec.txt
    
    echo 'waiting for kube cluster to stabilize'
    sleep 360
    
    sh ../attack-kubernetes/tearHR.sh
    echo 'waiting for cluster clean up'
    sleep 150
    echo 'ready to move the logs' 

    mkdir ./tracereceiver/test$reqpersec
    mv ./tracereceiver/*.tar ./tracereceiver/test$reqpersec/
done