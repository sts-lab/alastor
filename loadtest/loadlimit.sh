set -e -o pipefail

export OPENFAAS_URL=http://127.0.0.1:31112

#nworker=10
#duration=120s

nworker=3
duration=60s
reqpersec=5

#echo 'deploying hello-retail at 1-50-20'
#faas-cli deploy -f ../hello-retail.yaml

#echo 'deploying vanilla hello-retail at 1-50-20'
#faas-cli deploy -f ../hello-retail-vanilla.yaml

#echo 'waiting for hello-retail to come up...'
#sleep 30

#sh kubectlmetrics.sh &

totalreq=0

#for reqpersec in {1,5,10,15,20,25,30,35,40,45,50,55,60,65}
#do
    #echo test-worker$nworker.reqpersec$reqpersec.txt
    #without the for loop this runs with constant workload
for i in {7..20}
do
    echo 'deploying hello-retail at scale'
    faas-cli deploy -f ../hello-retail.yaml

    echo 'waiting for hello-retail to come up...'
    sleep 30

    echo 'launching hey for load testing'
    ../hey_linux_amd64 -z=$duration -q $reqpersec -c $nworker -m POST -D ../benign.json -H "Content-Type: application/json" http://127.0.0.1:31112/function/product-purchase > testperf-$i.txt
    tarsize=$(kubectl get pods --field-selector=status.phase==Running -o name -n openfaas-fn |cut -d"/" -f2| grep product-purchase|xargs -P 0 -I{} kubectl exec {} -n openfaas-fn -- /bin/sh -c "tar -czf iter$i{}.tar request.alastor* && stat -c "%s" iter$i{}.tar" | awk '{s+=$1} END {print s}')
    logsize=$(kubectl get pods --field-selector=status.phase==Running -o name -n openfaas-fn |cut -d"/" -f2| grep product-purchase|xargs -P 0 -I{} kubectl exec {} -n openfaas-fn -- /bin/sh -c 'du -skc request.*|grep total|awk "{print \$1*1000}"' | awk '{s+=$1} END {print s}')
    #kubectl get pods --field-selector=status.phase==Running -o name -n openfaas-fn |cut -d"/" -f2| grep product-purchase|xargs -P 8 -I{} kubectl exec {} -n openfaas-fn -- /bin/sh -c "curl -sS -F 'file=@iter$i{}.tar' http://172.22.224.34:44444/ && rm iter$i{}.tar"
    
    totalreq=$((totalreq+900))
    echo "${totalreq} ${tarsize} ${logsize}" >> loadlimit.dat
    sh ../attack-kubernetes/tearHR.sh
    echo 'waiting for cluster clean up'
    sleep 150
    echo 'ready to move the logs' 

    mkdir ./tracereceiver/test$i
    mv ./tracereceiver/*.tar ./tracereceiver/test$i/
done

echo 'stabilize the cluster'
sleep 300
sh ../attack-kubernetes/tearHR.sh
