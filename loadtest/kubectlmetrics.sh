#! /bin/bash
duration=2430
#duration=90
end=$((SECONDS+duration))
iter=0

echo "#Time tarsize logsize: Starting at $SECONDS" > loadlimit.dat

while [ $SECONDS -lt $end ]; do
    time=$SECONDS
    echo "Time is $SECONDS"
    #mkdir iter$iter
    tarsize=$(kubectl get pods --field-selector=status.phase==Running -o name -n openfaas-fn |cut -d"/" -f2| grep product-purchase|xargs -P 0 -I{} kubectl exec {} -n openfaas-fn -- /bin/sh -c "tar -czf iter$iter{}.tar request.alastor* && stat -c "%s" iter$iter{}.tar" | awk '{s+=$1} END {print s}')
    logsize=$(kubectl get pods --field-selector=status.phase==Running -o name -n openfaas-fn |cut -d"/" -f2| grep product-purchase|xargs -P 0 -I{} kubectl exec {} -n openfaas-fn -- /bin/sh -c 'du -skc request.*|grep total|awk "{print \$1*1000}"' | awk '{s+=$1} END {print s}')
    #kubectl get pods --field-selector=status.phase==Running -o name -n
    #openfaas-fn |cut -d"/" -f2| grep product-purchase|xargs -t -I{} kubectl cp
    #openfaas-fn/{}:test.tar ./iter$iter/{}.tar
    kubectl get pods --field-selector=status.phase==Running -o name -n openfaas-fn |cut -d"/" -f2| grep product-purchase|xargs -P 8 -I{} kubectl exec {} -n openfaas-fn -- /bin/sh -c "curl -sS -F 'file=@iter$iter{}.tar' http://172.22.224.34:44444/ && rm iter$iter{}.tar"
    iter=$((iter+1))
    echo "${time} ${tarsize} ${logsize}" >> loadlimit.dat
    sleep 60

    # append to file - <time> <output for first cmd> <output for second cmd>
done