#! /bin/bash

services='product-purchase-authorize-cc product-purchase-get-price product-purchase-publish product-purchase'
for service in $services
do
	kubectl delete service/$service deployment.apps/$service -n openfaas-fn
done