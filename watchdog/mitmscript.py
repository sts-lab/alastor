from mitmproxy import http
from mitmproxy import tcp
from pprint import pprint

def tcp_start(flow: tcp.TCPFlow) -> None:
    with open("outfile.txt", "a+") as ofile:
        pprint(flow,ofile)

def response(flow: http.HTTPFlow) -> None:
    
    with open("outfile.txt", "a+") as ofile:
        pprint(flow.request,ofile)
        pprint(flow.client_conn,ofile)
        #pprint(flow.request.host,ofile)
        pprint(flow.request.port,ofile)
        #pprint(flow.request.path,ofile)
        pprint(flow.request.headers,ofile)
        pprint(flow.request.timestamp_start,ofile)
        pprint(flow.request.timestamp_end,ofile)

        if flow.response.content:
            pprint(flow.response,ofile)
            pprint(flow.response.headers,ofile)
            pprint(flow.response.timestamp_start,ofile)
            pprint(flow.response.timestamp_end,ofile)

        # Add other separators etc. however you want
        ofile.write("-----------------------------------")