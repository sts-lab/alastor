#!/bin/bash

set -e

if [ ! $http_proxy == "" ] 
then
    docker build --no-cache --build-arg https_proxy=$https_proxy --build-arg http_proxy=$http_proxy -t dattapubali/watchdog:build .
else
    docker build --no-cache -t dattapubali/watchdog:build .
fi

docker create --name buildoutput dattapubali/watchdog:build echo
#docker create --name buildoutput openfaas/watchdog:build echo

#docker build --no-cache --build-arg PLATFORM="-armhf" -t openfaas/classic-watchdog:latest-dev-armhf . -f Dockerfile.packager
#docker build --no-cache --build-arg PLATFORM="-arm64" -t openfaas/classic-watchdog:latest-dev-arm64 . -f Dockerfile.packager
#docker build --no-cache --build-arg PLATFORM=".exe" -t openfaas/classic-watchdog:latest-dev-windows . -f Dockerfile.packager
docker build --no-cache --build-arg PLATFORM="" -t dattapubali/classic-watchdog:latest-dev-x86_64 . -f Dockerfile.packager

#docker create --name buildoutput openfaas/watchdog:build echo
#>>>>>>> 60fccd5992956cf36938749aae5fc8ec86bb160a

#docker cp buildoutput:/go/src/github.com/openfaas/faas/watchdog/watchdog ./fwatchdog
#docker cp buildoutput:/go/src/github.com/openfaas/faas/watchdog/watchdog-armhf ./fwatchdog-armhf
#docker cp buildoutput:/go/src/github.com/openfaas/faas/watchdog/watchdog-arm64 ./fwatchdog-arm64
#docker cp buildoutput:/go/src/github.com/openfaas/faas/watchdog/watchdog.exe ./fwatchdog.exe

#docker rm buildoutput

